from django.urls import path
from tweets import views

urlpatterns = [
    path('tweets/', views.TweetListCreateView.as_view(), name='tweet-list'),
    path('tweets/<int:pk>/', views.TweetRetrieveUpdateDeleteView.as_view(), name='tweet-detail'),
]
